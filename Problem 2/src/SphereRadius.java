//************************************************
// File:        SphereRadius
// Author:      Scott Royer
// Date:        09/26/2017
// Course:      CPS 100
//
// PROBLEM STATEMENT: Write an program that reads the radius of a sphere 
// and prints its volume and surface area. Use the following formulas. 
// Print the output to four decimal places. r represents the sphere's radius
// Volume = 4/3 pie r^3. 
// Surface Area = 4 pie r^2
// 
//INPUT:  User inputs a number representing the radius of a sphere 
//
// OUTPUT: This program displays on screen the following:
//         a) The surface area of a sphere to four decimal points
//         b) The volume of a sphere to four decimal points
//        
// 
//*************************************************

import java.util.Scanner;
import java.text.DecimalFormat;

public class SphereRadius {
  
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    DecimalFormat decForm = new DecimalFormat("0.####");
    double volume = 0;
    double area = 0;
    double radius = 0;
    
    System.out.println("Please type in the radius: ");
    radius = scan.nextDouble();
    scan.close();
    
    volume = (4.00/3) * Math.PI * Math.pow(radius, 3);
    area = 4* Math.PI * Math.pow(radius, 2);
    
    System.out.println("volume = " + decForm.format(volume));
    System.out.println("area = " + decForm.format(area));
    
    
  }
}
