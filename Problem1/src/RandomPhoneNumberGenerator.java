//************************************************
// File:        RandomPhoneNumber
// Author:      Scott Royer
// Date:        09/26/2017
// Course:      CPS 100
//
// PROBLEM STATEMENT: "Write an application that creates and prints 
// a random phone number of the form XXX-XXX-XXXX. Include the dashes in the output. 
// Do not let the first three digits contain an 8 or 9 (but don't be more restrictive than that), 
// and make sure that the second set of three digits is not greater than 655. 
//
// INPUT:  Requires no user input. Input is by parameters only.
//
// OUTPUT: A pseudo Random phone number in the form of XXX-XXX-XXXX
//*************************************************


import java.text.DecimalFormat;
import java.util.Random;

public class RandomPhoneNumberGenerator {
         public static void main(String[] args) {

        Random rand = new Random();
        int num1 = (rand.nextInt(8) * 100) + (rand.nextInt(8) * 10) + rand.nextInt(8);
        int num2 = rand.nextInt(655);
        int num3 = rand.nextInt(10000);

        DecimalFormat df1 = new DecimalFormat("000"); // 3 zeros
        DecimalFormat df2 = new DecimalFormat("0000"); // 4 zeros

        String phoneNumber = df1.format(num1) + "-" + df1.format(num2) + "-" + df2.format(num3);

        System.out.println(phoneNumber);
    }
}